module mycpu_top(
    /*
        模块名称: mycpu_top
        模块功能: 封装 CPU
        输入: 
            clk                 时钟信号
            resetn              复位信号
            inst_sram_rdata     从 inst_sram 读出的指令
            data_sram_rdata     从 data_sram 读出的数据
        输出:
            inst_sram_en        使能信号
            inst_sram_wen       写使能信号
            inst_sram_addr      读/写 inst_sram 的地址
            inst_sram_wdata     写入 inst_sram 的数据

            data_sram_en
            data_sram_wen
            data_sram_addr
            data_sram_wdata

            debug_wb_pc         pcW
            debug_wb_rf_wen     写寄存器堆使能信号
            debug_wb_rf_wnum    写寄存器堆的寄存器号
            debug_wb_rf_wdata   写寄存器堆的数据
    */
    input   wire            clk,
    input   wire            resetn,
    input   wire[5:0]       ext_int,
    //cpu inst sram
    output  wire            inst_sram_en,
    output  wire[3:0]       inst_sram_wen,
    output  wire[31:0]      inst_sram_addr,
    output  wire[31:0]      inst_sram_wdata,
    input   wire[31:0]      inst_sram_rdata,
    //cpu data sram
    output  wire            data_sram_en,
    output  wire[3:0]       data_sram_wen,
    output  wire[31:0]      data_sram_addr,
    output  wire[31:0]      data_sram_wdata,
    input   wire[31:0]      data_sram_rdata,
    // debug
    output  wire[31:0]      debug_wb_pc,
    output  wire[3:0]       debug_wb_rf_wen,
    output  wire[4:0]       debug_wb_rf_wnum,
    output  wire[31:0]      debug_wb_rf_wdata
    );

	wire [31:0]     pc;             // 读 inst_sram 的地址
	wire [31:0]     instr;          // 从 inst_sram 中读出的指令

	wire [3:0]      memwrite;       // data_sram 的写使能信号
	wire [31:0]     aluout;         // 读/写 data_sram 的地址
    wire [31:0]     writedata;      // 写入 data_sram 的数据
    wire [31:0]     readdata;       // 从 data_sram 中读出的数据

    wire [31:0]     pcW;
    wire            regwriteW;
    wire [4:0]      writeregW;
    wire [31:0]     resultW;

    wire [39:0]     ascii;

    mips mips(
        .clk(~clk),
        .rst(~resetn),
        .pc_pF(pc), 
        .instrF(instr), 
        .memwriteM(memwrite),
        .aluout_pM(aluout),
        .writedataM(writedata),
        .readdataM(readdata),
        .pcW(pcW),
        .regwrite(regwriteW),
        .writeregW(writeregW),
        .resultW(resultW)
    );

    // inst_sram
    assign inst_sram_en         = 1'b1;
    assign inst_sram_wen        = 4'b0;
    assign inst_sram_addr       = pc;
    assign inst_sram_wdata      = 32'b0;
    assign instr                = inst_sram_rdata;

    // data_sram
    assign data_sram_en         = 1'b1;
    assign data_sram_wen        = memwrite;
    assign data_sram_addr       = aluout;
    assign data_sram_wdata      = writedata;
    assign readdata             = data_sram_rdata;

    // debug
    assign debug_wb_pc          = pcW;
    assign debug_wb_rf_wen      = {4{regwriteW}};
    assign debug_wb_rf_wnum     = writeregW;
    assign debug_wb_rf_wdata    = resultW;

    // ascii
    instdec instdec(
        .instr(instr),
        .ascii(ascii)
    );

endmodule