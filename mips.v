`timescale 1ns / 1ps

module mips(
	/*
		输入:
			clk 			时钟信号
			rst 			复位信号
			instrF 			从 inst_sram 中读出的指令
			readdataM 		从 data_sram 中读出的数据
		输出:
			pc_pF 			读 inst_sram 的物理地址
			memwriteM 		data_sram 的写使能信号
			aluout_pM 		读/写 data_sram 的地址
			writedataM 		写入 data_sram 的数据
		debug:
			pcW
			regwriteW
			writeregW
			resultW
	*/
	input 	wire 			clk, rst,
	output 	wire[31:0] 		pc_pF,
	input 	wire[31:0] 		instrF,
	output 	wire[3:0] 		memwriteM,
	output 	wire[31:0] 		aluout_pM, writedataM,
	input 	wire[31:0] 		readdataM,

	output	wire[31:0]		pcW,
	output					regwrite,
	output	wire[4:0]		writeregW,
	output	wire[31:0]		resultW
    );
	
	wire[31:0]				instrD;
	wire 					regdstE, alusrcE, pcsrcD, memtoregE, memtoregM, memtoregW, regwriteW;
	wire					regwriteE, regwriteM;
	wire					hilotoregD, hiorloD, hilotoregE, hiorloE, hiwriteE, lowriteE, hiwriteM, lowriteM, hiwriteW, lowriteW, hilotoregM, hilotoregW;
	wire					immseD;
	wire					invalidD;
	wire					ismultE, ismultM, ismultW, isdivE, isdivM, isdivW, signedmultE, signeddivE;
	wire[5:0] 				alucontrolE;
	wire 					branchD, equalD, jumpD, jumpregD;
	wire					linkregE, linkdataW;
	wire					cp0toregE, cp0toregM, cp0toregW, cp0writeM;
	wire					stallE, flushE, stallM, flushM, stallW, flushW;
	wire[31:0]				pcF, aluoutM;
	wire					no_dcache;
	wire					is_overflow_detectM;

	assign regwrite = regwriteW | hilotoregW | cp0toregW;

	mmu mmu(
		.inst_vaddr(pcF),
		.inst_paddr(pc_pF),
		.data_vaddr(aluoutM),
		.data_paddr(aluout_pM),
		.no_dcache(no_dcache)
	);

	controller c(
		clk, rst,
		// ID
		instrD,
		equalD,
		pcsrcD, branchD, jumpD, jumpregD,
		hilotoregD, hiorloD,
		immseD,
		invalidD,
		// EX
		stallE, flushE,
		memtoregE, alusrcE,
		regdstE, regwriteE,	
		alucontrolE,
		hilotoregE, hiorloE, hiwriteE, lowriteE,
		ismultE, signedmultE,
		isdivE, signeddivE,
		linkregE,
		cp0toregE,
		// ME
		stallM, flushM,
		memtoregM, regwriteM,
		hiwriteM, lowriteM, hilotoregM,
		ismultM, isdivM,
		cp0toregM, cp0writeM,
		is_overflow_detectM,
		// WB
		stallW, flushW,
		memtoregW, regwriteW, 
		hiwriteW, lowriteW, hilotoregW,
		ismultW, isdivW,
		linkdataW,
		cp0toregW
	);

	datapath dp(
		clk, rst,
		// IF
		pcF,
		instrF,
		// ID
		pcsrcD, branchD, jumpD, jumpregD,
		hilotoregD, hiorloD,
		immseD,
		invalidD,
		equalD,
		instrD,
		// EX
		regdstE, alusrcE, memtoregE, regwriteE,
		hilotoregE, hiorloE, hiwriteE, lowriteE,
		alucontrolE,
		ismultE, signedmultE,
		isdivE, signeddivE,
		linkregE,
		cp0toregE,
		stallE, flushE,
		// ME
		memtoregM, regwriteM,
		hiwriteM, lowriteM, hilotoregM,
		ismultM, isdivM,
		readdataM,
		cp0toregM, cp0writeM,
		is_overflow_detectM,
		aluoutM, writedataM,
		memwriteM,
		stallM, flushM,
		// WB
		memtoregW, regwriteW,
		hiwriteW, lowriteW, hilotoregW,
		ismultW, isdivW,
		linkdataW,
		cp0toregW,
		pcW, writeregW, resultW,
		stallW, flushW
	);
	
endmodule
